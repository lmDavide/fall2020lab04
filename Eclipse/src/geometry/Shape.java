// David Tran 1938381

package geometry;

public interface Shape {
	
	double getArea();
	
	double getPerimeter();
}
