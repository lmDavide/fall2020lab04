// David Tran 1938381

package geometry;

public class Circle implements Shape {
	private double radius;
	
	public Circle(double radiusInput) {
		this.radius = radiusInput;
	}
	
	public double getRadius() {
		return this.radius;
	}
	
	public double getArea() {
		return (Math.PI * this.radius * this.radius);
		
	}
	
	public double getPerimeter() {
		return (2 * Math.PI * this.radius);
	}
}
