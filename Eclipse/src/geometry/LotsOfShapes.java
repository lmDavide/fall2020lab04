// David Tran 1938381

package geometry;

public class LotsOfShapes {

	public static void main(String[] args) {
		
		Shape[] shapeList = new Shape[5];
		Circle newShapeOne = new Circle(7);
		Circle newShapeTwo = new Circle(21);
		Rectangle newShapeThree = new Rectangle(2, 3);
		Rectangle newShapeFour = new Rectangle(10, 20);
		Square newShapeFive = new Square(5);
		shapeList[0] = newShapeOne;
		shapeList[1] = newShapeTwo;
		shapeList[2] = newShapeThree;
		shapeList[3] = newShapeFour;
		shapeList[4] = newShapeFive;
		
		for(int count = 0; count < shapeList.length; count++) {
			System.out.println("Shape Area: " + shapeList[count].getArea());
			System.out.println("Shape Perimeter: " + shapeList[count].getPerimeter());
		}
	}

}