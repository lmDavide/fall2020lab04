// David Tran 1938381

package geometry;

public class Rectangle implements Shape {
	private double length;
	private double width;
	
	public Rectangle(double lengthInput, double widthInput) {
		this.length = lengthInput;
		this.width = widthInput;
	}
	
	public double getInput() {
		return this.length;
	}
	
	public double getWidth() {
		return this.width;
	}
	
	public double getArea() {
		return (this.length * this.width);
	}
	
	public double getPerimeter() {
		return ( 2 * (this.length + this.width));
	}
}
