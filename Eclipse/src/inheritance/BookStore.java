// David Tran 1938381

package inheritance;

public class BookStore {

	public static void main(String[] args) {
		Book[] myBookStore = new Book[5];
		Book newBookOne = new Book("Cats", "David Tran");
		ElectronicBook newBookTwo = new ElectronicBook("Dogs", "Dog Lover", 64);
		Book newBookThree = new Book("Programming for Kids", "Dr. Prog");
		ElectronicBook newBookFour = new ElectronicBook("Programming for Kids", "Dr. Prog", 128);
		ElectronicBook newBookFive = new ElectronicBook("DIY with Kids", "DIY Master", 32);
		myBookStore[0] = newBookOne;
		myBookStore[1] = newBookTwo;
		myBookStore[2] = newBookThree;
		myBookStore[3] = newBookFour;
		myBookStore[4] = newBookFive;
		for(int count = 0; count < myBookStore.length; count++){
			System.out.println(myBookStore[count]);
		}
	}
}