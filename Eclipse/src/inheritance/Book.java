// David Tran 1938381

package inheritance;

public class Book {
	protected String title;
	private String author;
	
	public Book(String titleInput, String authorInput) {
		this.title = titleInput;
		this.author = authorInput;
	}
	
	public String getTitle() {
		return this.title;
	}
	public String getAuthor() {
		return this.author;
	}
	
	public String toString() {
		return ("Book Title: " + this.title + ", Book Author: " + this.author);
	}
}