// David Tran 1938381

package inheritance;

public class ElectronicBook extends Book{
	private double numberBytes;
	
	
	public ElectronicBook(String titleInput, String authorInput, double byteInput) {
		super(titleInput, authorInput);
		this.numberBytes = byteInput;
	}
	
	public String toString() {
		return ("Book Title: " + super.getTitle() + ", Book Author: " + super.getAuthor() + ", Number of Bytes: " + this.numberBytes);
	}
}